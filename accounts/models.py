from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class CustomUser(AbstractUser):

    picture = models.ImageField(verbose_name='写真', upload_to='profiles', blank=True, null=True)

    class Meta:
        verbose_name_plural = 'CustomUser'