//ajax通信に必要なcsrf関連の設定（ajax通信）

function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        var csrftoken = getCookie('csrftoken');

        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });


//動画再生ごとに再生数をカウント（ajax通信）
$("#startcount").click(function(event){
    event.preventDefault();
    var count = $(this);
    $.ajax({
    type: 'POST',
    url: count.attr('data-href'),
    data: {'csrfmiddlewaretoken': $("#csrdmiddlewaretoken").val()},
    dataType: 'json',
    })
});

//ドロップダウンのカテゴリー切り替えごとに表示する動画を変更（ajax通信）
$(function(){
    $('.dropdown-menu .dropdown-item').click(function(){
     var visibleItem = $('.dropdown-toggle', $(this).closest('.dropdown'));
     visibleItem.text($(this).attr('value'));
    });
});

$('.dropdown-menu .dropdown-item').click(function(event){
    var url = $("#dropdownMenuButton");
    $.ajax({
    type: 'POST',
    url: url.attr('data-href'),
    data: {'csrfmiddlewaretoken': $("#csrdmiddlewaretoken").val()},
    dataType: 'json',
    })
    .done(function(response){
        $('#content').html(response['form']);
    })
});

