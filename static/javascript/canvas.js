 function setup() {
    canvas = createCanvas(windowWidth / 2, windowHeight / 3);
    background(8, 91, 26);
    canvas.parent('canvas');
}

function touchMoved() {
    smooth();
    stroke(255);
    strokeWeight(4);
    line(mouseX, mouseY, pmouseX, pmouseY);
    return false;
}

function download() {
    var canvas = document.getElementById("defaultCanvas0");
    var link = document.getElementById("DLlink")
    link.href = canvas.toDataURL()

    // document.getElementById("canvasImage").src = canvas.toDataURL()

    link.click()
}