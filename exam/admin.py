from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Exam,Comment,Category,SubCategory,Channel,Register,Favorite,Blog,File,Question,Course,SubCourse,CourseRegister,UserAnswer



# Register your models here.
class BlogAdmin(SummernoteModelAdmin):
    summernote_fields = '__all__'

admin.site.register(Exam, BlogAdmin)
admin.site.register(Blog, BlogAdmin)
admin.site.register(UserAnswer)
admin.site.register(File)
admin.site.register(Question)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(Channel)
admin.site.register(Register)
admin.site.register(Favorite)
admin.site.register(Course)
admin.site.register(SubCourse)
admin.site.register(CourseRegister)
