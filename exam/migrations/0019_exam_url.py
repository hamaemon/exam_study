# Generated by Django 2.2.2 on 2021-01-10 15:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0018_file_file'),
    ]

    operations = [
        migrations.AddField(
            model_name='exam',
            name='url',
            field=models.URLField(blank=True, null=True, verbose_name='リンクURL'),
        ),
    ]
