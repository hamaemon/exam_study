# Generated by Django 2.2.2 on 2020-12-22 14:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0006_exam_start_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='exam',
            name='views',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
