import logging
from django.views.decorators.csrf import csrf_protect
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib import messages
from django.urls import reverse_lazy
from django.shortcuts import render
from django.shortcuts import redirect, get_object_or_404, get_list_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from .forms import InquiryForm,ExamCreateForm
from .models import Exam, Comment, Category,SubCategory,Channel,Register,Favorite,Blog,File,Question,Course,SubCourse,CourseRegister,UserAnswer
import urllib.request
import requests
from bs4 import BeautifulSoup


logger = logging.getLogger(__name__)

# Create your views here.
class IndexView(generic.TemplateView):
    template_name = "index.html"

class SubjectView(generic.ListView):
    model = Exam
    template_name = "subject.html"

    def get_queryset(self):
        subject = self.kwargs['category']
        return Exam.objects.filter(category__name=subject)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        category_select = get_object_or_404(Category, name=self.kwargs['category'])
        context['category_select']=category_select
        return context

class Subject2View(generic.ListView):
    model = Exam
    template_name = "subject2.html"

    def get_queryset(self):
        subject = self.kwargs['category']
        return Exam.objects.filter(category__name=subject)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        subject = self.kwargs['category']
        all = Exam.objects.filter(category__name=subject)
        category_select = get_object_or_404(Category, name=subject)
        context.update({
            'popular_list': all.order_by('-views'),
            'new_list': all.order_by('-created_at'),
            'category_select': category_select,
        })

        return context



class VideoView(generic.DetailView):
    model = Exam
    template_name = 'video.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        post = get_object_or_404(Exam,pk=self.kwargs["pk"])
        post.views += 1
        post.save()
        context.update({
            'tag_list': Exam.objects.get(pk=self.kwargs["pk"]).tags.similar_objects(),
            'comment_list': Comment.objects.filter(content=Exam.objects.get(pk=self.kwargs["pk"])),
        })
        return context


class InquiryView(generic.FormView):
    template_name = "inquiry.html"
    form_class = InquiryForm
    success_url = reverse_lazy('exam:inquiry')

    def form_valid(self,form):
        form.send_email()
        messages.success(self.request, 'メッセージを送信しました。')
        logger.info('Inquiry sent by {}'.format(form.cleaned_data['name']))
        return super().form_valid(form)

class ExamListView(LoginRequiredMixin,generic.ListView):
    model = Exam
    template_name = 'exam_list.html'
    paginate_by = 2

    def get_queryset(self):
        exams = Exam.objects.filter(user=self.request.user).order_by('-created_at')
        return exams

class  ExamDetailView(LoginRequiredMixin,generic.DetailView):
    model = Exam
    template_name = 'exam_detail.html'

class ExamCreateView(LoginRequiredMixin,generic.CreateView):
    model =Exam
    template_name = 'exam_create.html'
    form_class = ExamCreateForm
    success_url = reverse_lazy('exam:exam_list')

    def form_valid(self,form):
        exam = form.save(commit=False)
        exam.user = self.request.user
        exam.save()
        messages.success(self.request,'日記を作成しました。')
        return super().form_valid(form)

    def form_invalid(self,form):
        messages.error(self.request, '日記の作成に失敗しました。')
        return super().form_invalid(form)

class ExamUpdateView(LoginRequiredMixin,generic.UpdateView):
    model = Exam
    template_name = 'exam_update.html'
    form_class = ExamCreateForm

    def get_success_url(self):
        return reverse_lazy('exam:exam_detail', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self,form):
        messages.success(self.request,'日記を更新しました。')
        return super().form_valid(form)

    def form_invalid(self,form):
        messages.error(self.request, '日記の更新に失敗しました。')
        return super().form_invalid(form)

class ExamDeleteView(LoginRequiredMixin,generic.DeleteView):
    model = Exam
    template_name = 'exam_delete.html'
    success_url = reverse_lazy('exam:exam_list')

    def delete(self,request,*args,**kwargs):
        messages.success(self.request,'日記を削除しました。')
        return super().delete(request,*args,**kwargs)

def like(request,pk):
    post = get_object_or_404(Exam, pk=pk)
    post.good += 1
    post.save()
    return redirect('exam:video', pk=pk)


def startcount(request, pk):
    post =get_object_or_404(Exam, pk=pk)
    post.start_count += 1
    post.save()

def select(request,category,select ):
    all = Exam.objects.filter(category__name=category)
    selects = all.filter(tags__name__in=[select])
    context = {
        'select':selects,
    }

    if request.is_ajax():
        html = render_to_string('select.html', context, request=request)
        return JsonResponse({'form':html})

class MypageView(LoginRequiredMixin,generic.ListView):
    model = Exam
    template_name = 'mypage.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        user = self.request.user
        context.update({
            'mychannel_list': Channel.objects.filter(user=user),
            'registerchannel_list': Register.objects.filter(user=user),
            'favorite_list': Favorite.objects.filter(user=user),
        })

        return context

    def get_queryset(self):
        exams = Exam.objects.filter(user=self.request.user)
        return exams

    def post(self,request,*arg,**kwargs):
        user = self.request.user


class MychannelView(generic.ListView):
    model = Exam
    template_name = 'mychannel.html'

    def get_queryset(self):
        channel = get_object_or_404(Channel, pk=self.kwargs["pk"])
        mychannel = channel.name
        exams = Exam.objects.filter(channel__name=mychannel)
        return exams

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        mychannel = get_object_or_404(Channel, pk=self.kwargs["pk"])
        context.update({
            'mychannel': mychannel,
        })
        return context

class BlogView(generic.DetailView):
    model = Blog
    template_name = 'blog.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        post = get_object_or_404(Blog,pk=self.kwargs["pk"])
        post.views += 1
        post.save()
        context.update({

        })
        return context

class FileView(generic.DetailView):
    model = File
    template_name = 'file.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        post = get_object_or_404(File,pk=self.kwargs["pk"])
        post.views += 1
        post.save()
        context.update({

        })
        return context

def listfunc(request,pk):
   post = get_object_or_404(Exam, pk=pk)
   url = post.url

   response = requests.get(url)
   bs = BeautifulSoup(response.text, "html.parser")
   section = bs.find('section', {'id': 'tabpanelTopics1'})
   div = section.find('div')
   li_list = div.select('ul > li')
   # print(li_list)
   list=[]

   for li in li_list:
       # 記事のリンクを取得
       a = li.find('a')
       if a is None:
           continue
       url2 = a.get("href")

       # 記事のタイトルを取得
       span = a.select_one('h1 > span')
       if span is None:
           continue
       title = span.text

       list.append([title,url2])

   context = {'list': list,}
   return render(request, 'list.html', context)

class QuestionView(generic.DetailView):
    model = Question
    template_name = 'question.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        user = self.request.user
        post = get_object_or_404(Question,pk=self.kwargs["pk"])
        post.views += 1
        post.save()
        context.update({
            'question_list': Question.objects.filter(user=user),
        })
        return context


class MypageQuestionView(LoginRequiredMixin,generic.ListView):
    model = Question
    template_name = 'mypage_question.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        user = self.request.user
        course = get_object_or_404(CourseRegister, user=self.request.user)
        context.update({
            'mychannel_list': Channel.objects.filter(user=user),
            'registerchannel_list': Register.objects.filter(user=user),
            'favorite_list': Favorite.objects.filter(user=user),
            'mycourse': get_object_or_404(CourseRegister, user=self.request.user),
            'mysubcourse': SubCourse.objects.filter(course__name=course.course.name),
        })

        return context

    def get_queryset(self):
        register = get_object_or_404(CourseRegister, user=self.request.user)
        print(register)
        course2 = register.course.name
        questions= Question.objects.filter(course__name=course2)

        return questions

    def post(self,request,*arg,**kwargs):
        user = self.request.user


class CourseView(generic.ListView):
    model = Question
    template_name = 'mypage_course.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        user = self.request.user
        course = get_object_or_404(Course, pk=self.kwargs["pk"])
        context.update({
            'mychannel_list': Channel.objects.filter(user=user),
            'registerchannel_list': Register.objects.filter(user=user),
            'favorite_list': Favorite.objects.filter(user=user),
            'course': get_object_or_404(Course, pk=self.kwargs["pk"]),
            'mysubcourse': SubCourse.objects.filter(course__name=course.name),
            'subquestion': Question.objects.filter(course__name=course.name),
        })

        return context

    def get_queryset(self):
        register = get_object_or_404(CourseRegister, user=self.request.user)
        print(register)
        course2 = register.course.name
        questions= Question.objects.filter(course__name=course2)

        return questions



