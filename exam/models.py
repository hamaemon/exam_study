from accounts.models import CustomUser
from django.db import models
from taggit.managers import TaggableManager


# Create your models here.
class Category(models.Model):
    name = models.CharField('カテゴリー', max_length=50)
    slug = models.CharField('スラッグ', max_length=50, blank=True, null=True)

    def __str__(self):
        return self.name

class SubCategory(models.Model):
    name = models.CharField('カテゴリー', max_length=50)

    def __str__(self):
        return self.name

class Course(models.Model):
    name = models.CharField('コース名', max_length=50)


    def __str__(self):
        return self.name

class SubCourse(models.Model):
    name = models.CharField('サブコース名', max_length=50)
    course =models.ForeignKey(Course, verbose_name='コース', on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.name


class Channel(models.Model):
    user = models.ForeignKey(CustomUser, verbose_name='ユーザー', on_delete=models.PROTECT)
    name = models.CharField(verbose_name='チャンネル名', max_length=40)
    abstract = models.TextField(verbose_name='説明文', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='写真1', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)


    def __str__(self):
        return self.name


class Exam(models.Model):
    """動画モデル"""
    user = models.ForeignKey(CustomUser, verbose_name='ユーザー', on_delete=models.PROTECT)
    title = models.CharField(verbose_name='タイトル', max_length=40)
    content = models.TextField(verbose_name='本文', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='写真1', blank=True, null=True)
    document = models.FileField(verbose_name='動画1', upload_to='documents/', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)
    tags = TaggableManager(blank=True)
    good = models.IntegerField(default=0)
    start_count = models.PositiveIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, verbose_name='カテゴリー',on_delete=models.PROTECT,blank=True, null=True)
    subcategory = models.ManyToManyField(SubCategory, verbose_name='サブカテゴリー', blank=True, null=True)
    channel = models.ForeignKey(Channel, verbose_name='チャンネル名', on_delete=models.PROTECT, blank=True, null=True)
    url = models.URLField(verbose_name='リンクURL', blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Exam'

        def __str__(self):
            return self.title

class Blog(models.Model):
    """ブログモデル"""
    user = models.ForeignKey(CustomUser, verbose_name='ユーザー', on_delete=models.PROTECT)
    title = models.CharField(verbose_name='タイトル', max_length=40)
    content = models.TextField(verbose_name='本文', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='写真1', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)
    tags = TaggableManager(blank=True)
    good = models.IntegerField(default=0)
    start_count = models.PositiveIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, verbose_name='カテゴリー',on_delete=models.PROTECT,blank=True, null=True)
    subcategory = models.ManyToManyField(SubCategory, verbose_name='サブカテゴリー', blank=True, null=True)
    channel = models.ForeignKey(Channel, verbose_name='チャンネル名', on_delete=models.PROTECT, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Blog'

        def __str__(self):
            return self.title

class File(models.Model):
    """ファイルモデル"""
    user = models.ForeignKey(CustomUser, verbose_name='ユーザー', on_delete=models.PROTECT)
    title = models.CharField(verbose_name='タイトル', max_length=40)
    comment = models.TextField(verbose_name='コメント', blank=True, null=True)
    file = models.FileField(verbose_name='ファイル', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='サムネ画像', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)
    tags = TaggableManager(blank=True)
    good = models.IntegerField(default=0)
    start_count = models.PositiveIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, verbose_name='カテゴリー',on_delete=models.PROTECT,blank=True, null=True)
    subcategory = models.ManyToManyField(SubCategory, verbose_name='サブカテゴリー', blank=True, null=True)
    channel = models.ForeignKey(Channel, verbose_name='チャンネル名', on_delete=models.PROTECT, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'File'

        def __str__(self):
            return self.title

class Comment(models.Model):
    """コメントモデル"""
    user =models.ForeignKey(CustomUser, verbose_name='ユーザー', on_delete=models.PROTECT)
    content = models.ForeignKey(Exam, verbose_name='コンテンツ', on_delete=models.PROTECT)
    comment = models.TextField(verbose_name='コメント', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)

    class Meta:
        verbose_name_plural = 'Comment'

class Register(models.Model):
    """チャンネル登録モデル"""
    user = models.ForeignKey(CustomUser, verbose_name='登録者', on_delete=models.PROTECT)
    channel = models.ForeignKey(Channel, verbose_name='登録チャンネル', on_delete=models.PROTECT)

class CourseRegister(models.Model):
    """コース登録モデル"""
    user = models.ForeignKey(CustomUser, verbose_name='コース登録者', on_delete=models.PROTECT)
    course = models.ForeignKey(Course, verbose_name='登録コース', on_delete=models.PROTECT)

class Favorite(models.Model):
    """お気に入りモデル"""
    user = models.ForeignKey(CustomUser, verbose_name='お気に入り登録者', on_delete=models.PROTECT)
    video = models.ForeignKey(Exam, verbose_name='登録動画', on_delete=models.PROTECT)

class Question(models.Model):
    """問題モデル"""
    user = models.ForeignKey(CustomUser, verbose_name='ユーザー', on_delete=models.PROTECT)
    title = models.CharField(verbose_name='タイトル', max_length=40)
    comment = models.TextField(verbose_name='コメント', blank=True, null=True)
    question = models.FileField(verbose_name='問題', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='サムネ画像', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='更新日時', auto_now=True)
    tags = TaggableManager(blank=True)
    good = models.IntegerField(default=0)
    start_count = models.PositiveIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, verbose_name='カテゴリー',on_delete=models.PROTECT,blank=True, null=True)
    subcategory = models.ManyToManyField(SubCategory, verbose_name='サブカテゴリー', blank=True, null=True)
    channel = models.ForeignKey(Channel, verbose_name='チャンネル名', on_delete=models.PROTECT, blank=True, null=True)
    course = models.ForeignKey(Course, verbose_name='コース名',on_delete=models.PROTECT,blank=True, null=True)
    subcourse = models.ForeignKey(SubCourse, verbose_name='サブコース名',on_delete=models.PROTECT,blank=True, null=True)
    difficult = models.IntegerField(verbose_name='難易度', default=0)
    important = models.IntegerField(verbose_name='重要度', default=0)
    time = models.IntegerField(verbose_name='目標解答時間', default=0)


    class Meta:
        verbose_name_plural = 'Question'

        def __str__(self):
            return self.title

class UserAnswer(models.Model):
    """解答状況モデル"""
    user = models.ForeignKey(CustomUser, verbose_name='解答ユーザー', on_delete=models.PROTECT)
    answer = models.ForeignKey(Question, verbose_name='解答問題', on_delete=models.PROTECT)
    collect = models.IntegerField(verbose_name='正誤', blank=True, null=True)
    memo = models.TextField(verbose_name='メモ', blank=True, null=True)
    photo = models.ImageField(verbose_name='解答画像', blank=True, null=True)
    point = models.IntegerField(verbose_name='点数', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='作成日時', auto_now_add=True)
    time = models.IntegerField(verbose_name='答時間', default=0)