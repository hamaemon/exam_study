from django.urls import path
from . import views

app_name = 'exam'
urlpatterns = [
    path('', views.IndexView.as_view(),name="index"),
    path('subject/<str:category>/', views.SubjectView.as_view(),name='subject'),
    path('subject2/<str:category>', views.Subject2View.as_view(),name='subject2'),
    path('video/<int:pk>',views.VideoView.as_view(),name='video'),
    path('inquiry/', views.InquiryView.as_view(),name="inquiry"),
    path('exam-list/', views.ExamListView.as_view(), name='exam_list'),
    path('exam-detail/<int:pk>/', views.ExamDetailView.as_view(), name='exam_detail'),
    path('exam-create/', views.ExamCreateView.as_view(), name='exam_create'),
    path('exam-update/<int:pk>', views.ExamUpdateView.as_view(), name='exam_update'),
    path('exam-delete/<int:pk>', views.ExamDeleteView.as_view(), name='exam_delete'),
    path('video/<int:pk>/like/', views.like, name='like'),
    path('video/<int:pk>/startcount/',views.startcount, name='startcount'),
    path('subject2/<str:category>/<str:select>/',views.select, name='select'),
    path('mypage/', views.MypageView.as_view(), name="mypage"),
    path('mychannel/<int:pk>/', views.MychannelView.as_view(), name="mychannel"),
    path('blog/<int:pk>/',views.BlogView.as_view(), name='blog'),
    path('file/<int:pk>/',views.FileView.as_view(), name='file'),
    path('list/<int:pk>/', views.listfunc, name='list'),
    path('question/<int:pk>/', views.QuestionView.as_view(), name='question'),
    path('mypage-question/', views.MypageQuestionView.as_view(), name="mypage_question"),
    path('course/<int:pk>', views.CourseView.as_view(), name="course"),
]